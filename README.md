# 银行转账系统示例

- RESTful 风格

- 核心框架: Spring Boot

- 模板引擎: Thymeleaf 3

- 前端框架: Vue + Jquery + Bootstrap


## 运行效果:
![银行转账系统示例](bank.gif)
