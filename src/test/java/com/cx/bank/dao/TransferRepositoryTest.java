package com.cx.bank.dao;

import com.cx.bank.BankApplication;
import com.cx.bank.domain.Transfer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * TransferRepositoryTest
 *
 * @author LongShu 2017/06/16
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BankApplication.class)
public class TransferRepositoryTest {

    @Autowired
    private TransferRepository transferRepository;

    @Test
    public void save() {
        Transfer transfer = new Transfer();
        transfer.setFromCard("000000").setToCard("111111").setMoney(1024.1024);

        transfer = transferRepository.saveAndFlush(transfer);

        System.out.println("transfer = " + transfer);

        findAll();
    }

    public void findAll() {
        System.out.println("TransferRepositoryTest.findAll");
        List<Transfer> transferList = transferRepository.findAll();
        for (Transfer transfer : transferList) {
            System.out.println("transfer = " + transfer);
        }
    }

}