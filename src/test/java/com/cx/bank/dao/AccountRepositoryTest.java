package com.cx.bank.dao;

import com.cx.bank.BankApplication;
import com.cx.bank.domain.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * AccountRepositoryTest
 *
 * @author LongShu 2017/06/15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BankApplication.class)
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository repository;

    @Test
    public void save() {
        Account account = new Account();
        account.setCard("000000").setName("lonsghu").setPassword("123456")
                .setMoney(1024 << 10).setCreateTime(new Date());

        repository.save(account);

        account.setId(null).setCard("111111");
        account = repository.saveAndFlush(account);
        System.out.println("account = " + account);
    }

    @Test
    public void delete() {
        try {
            repository.delete(1);
        } catch (DataAccessException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void update() {
        Account account = repository.findByCard("111111");
        System.out.println("account = " + account);
        if (account == null) {
            return;
        }
        account.setMoney(10240.1024);
        account = repository.save(account);
        System.out.println("account = " + account);
    }

    @Test
    public void findAll() {
        List<Account> accountList = repository.findAll();
        for (Account account : accountList) {
            System.out.println("account = " + account);
        }
    }

    @Test
    public void findByAccount() {
        Account account = repository.findByCard("111111");
        System.out.println("account = " + account);
    }

}