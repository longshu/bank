package com.cx.bank.manager;

import com.cx.bank.BankApplication;
import com.cx.bank.domain.Account;
import com.cx.bank.domain.Transfer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * AccountManagerTest
 *
 * @author LongShu 2017/06/15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BankApplication.class)
public class AccountManagerTest {
    private static Logger logger = LoggerFactory.getLogger(AccountManagerTest.class);

    @Autowired
    private AccountManager accountManager;

    private String testAccount = "123456";

    @Test
    public void login() throws Exception {
        Account account = accountManager.login(testAccount, testAccount);
        logger.info("account:{}", account);
    }

    @Test
    public void register() throws Exception {
        Account account = new Account();
        account.setCard(testAccount).setPassword(testAccount).setName("longshu")
                .setMoney(10240.24).setCreateTime(new Date());

        account = accountManager.register(account);

        logger.info("account:{}", account);
    }

    @Test
    public void queryAccount() throws Exception {
        Account account = accountManager.queryAccount(testAccount);

        logger.info("account:{}", account);
    }

    @Test
    public void deposit() throws Exception {
        double money = accountManager.deposit(testAccount, 2048.2);
        logger.info("money:{}", money);
    }

    @Test
    public void withdrawals() throws Exception {
        double money = accountManager.withdrawals(testAccount, 1024.1);
        logger.info("money:{}", money);
    }

    @Test
    public void transfer() throws Exception {
        Transfer transfer = new Transfer();
        transfer.setFromCard(testAccount).setToCard(testAccount).setMoney(1024);

        accountManager.transfer(transfer);
    }

}