package com.cx.bank;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.Serializable;

/**
 * BankConfiguration
 *
 * @author LongShu 2017/06/15
 */
@Configuration
@EnableCaching
@Order(Ordered.HIGHEST_PRECEDENCE)
@ComponentScan("com.cx.bank")
@EntityScan(basePackages = "com.cx.bank.domain")
@EnableJpaRepositories(basePackages = "com.cx.bank.dao")
public class BankConfiguration implements Serializable {
    private static final long serialVersionUID = 1L;

}
