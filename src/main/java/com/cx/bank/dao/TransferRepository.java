package com.cx.bank.dao;

import com.cx.bank.domain.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TransferRepository
 *
 * @author LongShu 2017/06/16
 */
@Repository
public interface TransferRepository extends JpaRepository<Transfer, Integer> {

    List<Transfer> findByFromCard(String fromCard);

    List<Transfer> findByToCard(String toCard);

}
