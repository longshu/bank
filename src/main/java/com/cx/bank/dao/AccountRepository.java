package com.cx.bank.dao;

import com.cx.bank.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * AccountRepository
 *
 * @author LongShu 2017/06/15
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    /**
     * 查找账户
     *
     * @param card 唯一的账号
     */
    Account findByCard(String card);

}
