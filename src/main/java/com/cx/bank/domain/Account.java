package com.cx.bank.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Account
 *
 * @author LongShu 2017/06/15
 */
@Entity
@Table(name = "account")
public class Account extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;

    /**
     * 账号
     */
    @Column(name = "card", unique = true, nullable = false, length = 32)
    private String card;

    /**
     * 账户密码
     */
    @Column(name = "password", nullable = false, length = 128)
    private String password;

    /**
     * 用户姓名
     */
    @Column(name = "name", nullable = false, length = 32)
    private String name;

    /**
     * 账户的钱,单位元
     */
    @Column(name = "money", precision = 20, scale = 2)// decimal(20,2)
    private BigDecimal money = BigDecimal.ZERO;

    /**
     * 账户创建时间
     */
    @Column(name = "createTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Transfer.class)
    @JoinColumn(name = "fromCard", referencedColumnName = "card")
    private List<Transfer> transferRecord = new LinkedList<>();

    @Override
    public int hashCode() {
        return Objects.hash(id, card, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Account)) return false;
        Account acc = (Account) o;
        return Objects.equals(id, acc.id) &&
                Objects.equals(card, acc.card) &&
                Objects.equals(name, acc.name);
    }

    public Integer getId() {
        return id;
    }

    public Account setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getCard() {
        return card;
    }

    public Account setCard(String card) {
        this.card = card;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Account setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getName() {
        return name;
    }

    public Account setName(String name) {
        this.name = name;
        return this;
    }

    public BigDecimal getMoney() {
        if (money.scale() != 2) {
            money = money.setScale(2, BigDecimal.ROUND_HALF_UP);
        }
        return money;
    }

    public Account setMoney(BigDecimal money) {
        this.money = money;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Account setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public List<Transfer> getTransferRecord() {
        return transferRecord;
    }

    public Account setTransferRecord(List<Transfer> transferRecord) {
        this.transferRecord = transferRecord;
        return this;
    }

    /**
     * 设置金额 四舍五入
     */
    public Account setMoney(double money) {
        this.money = BigDecimal.valueOf(money).setScale(2, BigDecimal.ROUND_HALF_UP);
        return this;
    }

}
