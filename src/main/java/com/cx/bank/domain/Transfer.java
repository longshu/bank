package com.cx.bank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Transfer
 *
 * @author LongShu 2017/06/16
 */
@Entity
@Table(name = "transfer")
public class Transfer extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 账号源
     */
    @Column(name = "fromCard", nullable = false, length = 32)
    private String fromCard;

    /**
     * 账号目的
     */
    @Column(name = "toCard", nullable = false, length = 32)
    private String toCard;

    @Column(name = "money", precision = 20, scale = 2)
    private BigDecimal money = BigDecimal.ZERO;

    /**
     * 转账时间
     */
    @Column(name = "createTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return Objects.equals(id, transfer.id) &&
                Objects.equals(fromCard, transfer.fromCard) &&
                Objects.equals(toCard, transfer.toCard) &&
                Objects.equals(money, transfer.money);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fromCard, toCard, money);
    }

    public Integer getId() {
        return id;
    }

    public Transfer setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getFromCard() {
        return fromCard;
    }

    public Transfer setFromCard(String fromCard) {
        this.fromCard = fromCard;
        return this;
    }

    public String getToCard() {
        return toCard;
    }

    public Transfer setToCard(String toCard) {
        this.toCard = toCard;
        return this;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public Transfer setMoney(BigDecimal money) {
        this.money = money;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Transfer setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 设置金额 四舍五入
     */
    public Transfer setMoney(double money) {
        this.money = BigDecimal.valueOf(money).setScale(2, BigDecimal.ROUND_HALF_UP);
        return this;
    }

}
