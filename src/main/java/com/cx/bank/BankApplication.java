package com.cx.bank;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "com.cx.bank")
public class BankApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return configureApplication(application);
    }

    public static void main(String[] args) {
        // SpringApplication.run(BankApplication.class, args);
        configureApplication(new SpringApplicationBuilder()).run(args);
        System.out.println("BankApplication running...");
    }

    public static SpringApplicationBuilder configureApplication(SpringApplicationBuilder application) {
        return application.sources(BankApplication.class).bannerMode(Banner.Mode.CONSOLE);
    }

}
