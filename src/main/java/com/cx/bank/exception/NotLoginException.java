package com.cx.bank.exception;

/**
 * NotLoginException
 *
 * @author LongShu 2017/06/15
 */
public class NotLoginException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public NotLoginException() {
    }

    public NotLoginException(String message) {
        super(message);
    }

    public NotLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotLoginException(Throwable cause) {
        super(cause);
    }

}
