package com.cx.bank.exception;

/**
 * AccountException
 *
 * @author LongShu 2017/06/15
 */
public class AccountException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public AccountException() {
    }

    public AccountException(String message) {
        super(message);
    }

    public AccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountException(Throwable cause) {
        super(cause);
    }

}
