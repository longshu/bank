package com.cx.bank.exception;

/**
 * AccountOverDrawnException 取款超出余额时抛出异常
 *
 * @author LongShu 2017/06/15
 */
public class AccountOverDrawnException extends AccountException {
    private static final long serialVersionUID = 1L;

    public AccountOverDrawnException() {
    }

    public AccountOverDrawnException(String message) {
        super(message);
    }

    public AccountOverDrawnException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountOverDrawnException(Throwable cause) {
        super(cause);
    }

}
