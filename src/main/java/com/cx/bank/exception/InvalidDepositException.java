package com.cx.bank.exception;

/**
 * InvalidDepositException 存款为负数时抛出异常
 *
 * @author LongShu 2017/06/15
 */
public class InvalidDepositException extends AccountException {
    private static final long serialVersionUID = 1L;

    public InvalidDepositException() {
    }

    public InvalidDepositException(String message) {
        super(message);
    }

    public InvalidDepositException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidDepositException(Throwable cause) {
        super(cause);
    }

}
