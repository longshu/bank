package com.cx.bank.controller;

import com.cx.bank.domain.Account;
import com.cx.bank.domain.Transfer;
import com.cx.bank.manager.AccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * AccountController
 *
 * @author LongShu 2017/06/15
 */
@RestController
@RequestMapping("/account")
public class AccountController extends BaseController {

    @Autowired
    private AccountManager manager;

    @PostMapping("/login")
    public Object login(HttpServletRequest request, String card, String password) {
        Account account = manager.login(card, password);
        account.setPassword(null);
        setSessionAttribute(request, SESSION_USER_KEY, account);
        return WebData.newMsg("登录成功", account);
    }

    @PostMapping("/register")
    public Object register(@RequestBody Account account) {
        logger.debug("account:{}", account);

        account = manager.register(account);
        account.setPassword(null);
        return WebData.newMsg("注册成功", account);
    }

    @GetMapping("/")
    public Object queryAccount(HttpServletRequest request) {
        Account account = getLoginUser(request);
        account = manager.queryAccount(account.getCard());
        account.setPassword(null);
        return WebData.newObj(account);
    }

    @PostMapping("/deposit")
    public Object deposit(HttpServletRequest request, Double money) {
        Account account = getLoginUser(request);
        double m = manager.deposit(account.getCard(), money);
        return WebData.newMsg("存款成功", m);
    }

    @PostMapping("/withdrawals")
    public Object withdrawals(HttpServletRequest request, Double money) {
        Account account = getLoginUser(request);
        double m = manager.withdrawals(account.getCard(), money);
        return WebData.newMsg("取款成功", m);
    }

    @PostMapping("/transfer")
    public Object transfer(HttpServletRequest request, @RequestBody Transfer transfer) {
        logger.debug("transfer:{}", transfer);
        Account account = getLoginUser(request);
        if (account.getCard().equals(transfer.getToCard())) {
            return WebData.newError("转给自己干嘛!");
        }

        transfer.setFromCard(account.getCard());
        transfer = manager.transfer(transfer);
        return WebData.newMsg("转账成功", transfer);
    }

    @GetMapping("/transferRecord")
    public Object transferRecord(HttpServletRequest request) {
        Account account = getLoginUser(request);
        List<Transfer> transferList = manager.transferRecord(account.getCard());

        return WebData.newObj(transferList);
    }

    @RequestMapping("/exit")
    public Object exit(HttpSession session) {
        session.invalidate();

        return new ModelAndView("/login");
    }

}
