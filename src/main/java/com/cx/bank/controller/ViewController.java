package com.cx.bank.controller;

import com.cx.bank.util.ConvertUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * ViewController
 *
 * @author LongShu 2017/06/15
 */
@Controller
@RequestMapping("/")
public class ViewController extends BaseController {

    @RequestMapping("/")
    public String home() {
        return "/index";
    }

    @RequestMapping("/view/{v}")
    public String view(@PathVariable(value = "v", required = false) String view) {
        logger.debug("view:{}", view);
        return view;
    }

    @RequestMapping("/error/{code}")
    public String error(@PathVariable(required = false) String code, HttpServletResponse response) {
        logger.debug("code:{}", code);
        Integer keyCode = ConvertUtil.toInt(code, 404);
        String view = "/error/" + keyCode;
        if ("/error/404".equals(view)) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            response.setStatus(keyCode);
        }
        return view;
    }

}
