package com.cx.bank.manager;

import com.cx.bank.domain.Account;
import com.cx.bank.domain.Transfer;
import com.cx.bank.exception.AccountException;
import com.cx.bank.exception.ServiceException;

import java.util.List;

/**
 * AccountManager
 *
 * @author LongShu 2017/06/15
 */
public interface AccountManager {

    /**
     * 登录
     *
     * @param card     账号
     * @param password 密码
     * @return
     * @throws ServiceException
     */
    Account login(String card, String password) throws ServiceException;

    /**
     * 注册
     *
     * @param account 账号信息
     * @return 完整账号信息
     * @throws ServiceException
     */
    Account register(Account account) throws ServiceException;

    /**
     * 查询账户
     *
     * @param card 账号
     * @return
     * @throws ServiceException
     */
    Account queryAccount(String card) throws ServiceException;

    /**
     * 存款
     *
     * @param card  账号
     * @param money 存款金额
     * @return 当前金额
     * @throws AccountException
     */
    double deposit(String card, double money) throws ServiceException;

    /**
     * 取款
     *
     * @param card  账号
     * @param money 取款金额
     * @return 当前金额
     * @throws AccountException
     */
    double withdrawals(String card, double money) throws ServiceException;

    /**
     * @param transfer 转账信息
     */
    Transfer transfer(Transfer transfer) throws ServiceException;

    List<Transfer> transferRecord(String card);

}
