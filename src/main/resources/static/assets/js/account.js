/**
 * userInfo
 *
 * @author LongShu 2017/06/16
 */
(function () {
    var accountInfo = new Vue({
        el: "#accountInfo",
        data: {
            msg: null,
            money: 99.99,
            accountInfo: {
                transferRecord: []
            },
            transferModel: {
                fromCard: '',
                toCard: '',
                money: 0
            }
        },
        methods: {
            getAccountInfo: function () {
                this.$http.get(fixUrl("account/")).then(function (response) {
                    $('.alert').hide();
                    var webData = new WebData(response.body);
                    if (webData.isError()) {
                        this.showMsg(webData.msg);
                        return false;
                    }
                    this.accountInfo = webData.data;
                    this.transferModel.fromCard = this.accountInfo.card;
                }, function (response) {
                    console.warn(response.body);
                    this.showMsg("获取账户信息失败!");
                });
            },
            deposit: function () {
                if (!this.money) {
                    this.showMsg("请输入金额!");
                    return false;
                }
                this.$http.post(fixUrl('account/deposit'), {money: this.money}, {
                    emulateJSON: true
                }).then(function (response) {
                    var webData = new WebData(response.body);
                    if (webData.isError()) {
                        this.showMsg(webData.msg);
                        return false;
                    }
                    this.accountInfo.money = webData.data;
                    this.showMsg(webData.msg);
                }, function (response) {
                    console.warn(response.body);
                    this.showMsg(response.body.error);
                    alert("系统错误,存款失败!");
                });
            },
            withdrawals: function () {
                if (!this.money) {
                    this.showMsg("请输入金额!");
                    return false;
                }
                this.$http.post(fixUrl('account/withdrawals'), {money: this.money}, {
                    emulateJSON: true
                }).then(function (response) {
                    var webData = new WebData(response.body);
                    if (webData.isError()) {
                        this.showMsg(webData.msg);
                        return false;
                    }
                    this.accountInfo.money = webData.data;
                    this.showMsg(webData.msg);
                }, function (response) {
                    console.warn(response.body);
                    this.showMsg(response.body.error);
                    alert("系统错误,取款失败!");
                });
            },
            transfer: function () {
                this.transferModel.money = this.money;
                if (!this.transferModel.toCard) {
                    this.showMsg("请输入对方卡号!");
                    return false;
                }
                this.$http.post(fixUrl('account/transfer'), this.transferModel).then(function (response) {
                    var webData = new WebData(response.body);
                    if (webData.isError()) {
                        this.showMsg(webData.msg);
                        return false;
                    }
                    // 保留两位小数
                    this.accountInfo.money = (this.accountInfo.money - this.money).toFixed(2);
                    this.accountInfo.transferRecord.unshift(webData.data);
                    this.showMsg(webData.msg);
                }, function (response) {
                    console.warn(response.body);
                    this.showMsg(response.body.error);
                    alert("系统错误,取款失败!");
                });
            },
            showMsg: function (msg) {
                this.msg = msg;
                $('.alert').show();
            }
        }
    });

    accountInfo.getAccountInfo();
})();
