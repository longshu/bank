/**
 * login.js
 *
 * @author LongShu 2017/06/16
 */
(function () {
    var login_form = new Vue({
        el: '#login_form',
        data: {
            accountModel: {
                card: '',
                password: ''
            }
        },
        methods: {
            login: function () {
                this.$http.post(fixUrl('account/login'), this.accountModel, {
                    emulateJSON: true
                }).then(function (response) {
                    var webData = new WebData(response.body);
                    if (webData.isError()) {
                        alert(webData.msg);
                        return false;
                    }
                    alert(webData.data.name + webData.msg);
                    if (webData.isMsg()) {
                        Cookies.set('card', login_form.accountModel.card, {expires: 7, path: '/'});
                        toLocation('');
                    }
                }, function (response) {
                    console.warn(response.body);
                    alert("系统错误,登录失败!");
                });
            }
        }// methods
    });

    var card = Cookies.get("card");
    if (card) {
        login_form.accountModel.card = card;
    }
})();
