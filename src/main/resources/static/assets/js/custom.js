/**
 * 获取url参数值
 * @param name
 * @returns
 */
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r)
        return decodeURI(r[2]);
    return null;
};
/**
 * 判断obj是否是jQuery对象
 */
function isJQuery(obj) {
    try {
        return obj instanceof jQuery;
    } catch (e) {
        console.error(e);
        return false;
    }
}
/**
 * 判断是否是DOM对象
 */
function isDOM(obj) {
    return obj instanceof HTMLElement;
}

var basePath = document.getElementById("base").title;
function fixUrl(url) {
    if (url) {
        if (url.indexOf(basePath) === 0) {
            return url;
        }
        return basePath + url;
    }
    return basePath;
}

/**
 * 页面跳转
 */
function toLocation(url) {
    if (undefined === url) {
        return toLocation("error/404");
    } else {
        return document.location.href = fixUrl(url);
    }
}

function sleep(millis) {
    for (var t = Date.now(); Date.now() - t <= millis;);
}
