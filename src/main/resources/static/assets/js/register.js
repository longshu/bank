/**
 * register.js
 *
 * @author LongShu 2017/06/16
 */
var register_form = new Vue({
    el: '#register_form',
    data: {
        accountModel: {
            card: '',
            name: '',
            password: '',
            confirm_password: ''
        }
    },
    methods: {
        register: function () {
            if (this.accountModel.password !== this.accountModel.confirm_password) {
                alert("密码不一致!");
                return false;
            }
            this.$http.post(fixUrl('account/register'), this.accountModel)
                .then(function (response) {
                    var webData = new WebData(response.body);
                    alert(webData.msg);
                    if (webData.isMsg()) {
                        toLocation('/view/login')
                    }
                }, function (response) {
                    console.warn(response.body);
                    alert("注册失败!");
                });
        }
    }
});
