/**
 * WebData
 */
function WebData(data) {
    this.result = this.MSG;
    this.msg = null;
    this.type = 'text'; // 'text', 'json', 'xml'
    this.data = null;

    if (data) {
        this.result = data.result;
        this.msg = data.msg;
        this.type = data.type;
        this.data = data.data;
    }

}

WebData.prototype = {
    ERROR: -1, // 错误
    MSG: 1, // 文本信息/消息
    OBJ: 2, // json对象或数组
    isError: function () {
        return this.ERROR === this.result;
    },
    isMsg: function () {
        return this.MSG === this.result;
    },
    isObj: function () {
        return this.OBJ === this.result;
    }
}
